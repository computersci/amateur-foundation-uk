# Amateur Radio Lessons - Foundation UK

Foundation License Lessons for UK Ofcom regulations. Provided free of charge, although not guaranteed to be up to date.

See [the wiki](https://gitlab.com/computersci/amateur-foundation-uk/wikis/home) for the lessons
